'''
@file MotorDriver.py
@brief Contains the class that controls the action of a motor
@details The full code can be found at https://bitbucket.org/bbons/me-405-lab-8/src/master/MotorDriver.py

@author Ben Bons and Ben Presley

@date Last Updated: March 10, 2021


'''
import pyb
import micropython
import utime

class MotorDriver:
    '''
    @brief Controls a DC electric motor Using PWM control through the DRV8847 motor driver
    @details This class will configure the timer and the pins for controlling the motor. 
    This class allows for operation of all 4 functions of the DRV8847 motor driver: forward, 
    reverse, coast (fast decay), and brake (slow decay). The class also controls the nSLEEP pin
    which will enable and disable all motors connected to the motor driver. 
    '''
    
        
    def __init__(self, nSLEEP_pin, nFAULT_pin, IN1_pin, IN1_chan, IN2_pin, IN2_chan, timer):
        '''
        @brief Sets the pins, channels and timer associated with the PWM control of the Motors
        @details This constructor takes all necessary pins and timers so that the motors can be
        controlled by pulse width modulation. It is necessary to make sure that the timer, pins and
        channels all match up for the pins and the correct timer is used. 
        
        @param nSLEEP_pin The pyb.Pin reference for the cpu pin tied to the nSLEEP pin of the DRV8847
        @param nFAULT_pin The pyb.Pin reference for the cpu pin tied to the nFAULT pin of the DRV8847 
        @param IN1_pin The pyb.Pin reference for the cpu pin tied to IN1 or IN3 of the DRV8847
        @param IN1_chan The channel number associated with the pin for the IN1_pin parameter above
        @param IN2_pin The pyb.Pin reference for the cpu pin tied to IN2 if using IN1 or IN4 if using IN3 of the DRV8847
        @param IN2_chan The channel number associated with the pin for the IN2_pin parameter above
        @param timer The pyb.Timer object associated with the pins and channels. The timer frequency must be high enough so the motor can effectively filter it out. 
        '''
        ## The pyb.Pin object that is set to be pushed low or pulled high, disabling or enabling the entire DRV8847
        self.nSLEEP_pin = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        
        
        
        if nFAULT_pin:
            ## The pyb.Pin object that is set to take digital input. It is connected to an open-drain fault detection pin on the DRV8847
            self.nFAULT_pin = pyb.Pin(nFAULT_pin, mode = pyb.Pin.IN)
            
            self._ext = pyb.ExtInt(pyb.Pin(nFAULT_pin, mode = pyb.Pin.IN), pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self._fault)
        self._isFault = False
        
        ## The pyb.Timer object that is used to control the PWM signal
        self.timer = timer
        
        ## The timerChannel object that outputs the PWM signal on the IN1 or IN3 pin
        self.motor_Channel_A = self.timer.channel(IN1_chan, pyb.Timer.PWM, pin = IN1_pin)
        
        ## The timerChannel object that outputs the PWM signal on the IN2 or IN4 pin
        self.motor_Channel_B = self.timer.channel(IN2_chan, pyb.Timer.PWM, pin = IN2_pin)
        
        ## The flag variable that is used to specify the motor's current direction, with 0 = forward, 1 = reverse.
        self.direction = 0
        
    def _fault(self, pin):
        self.disable()
        self.set_duty(0)
        print("FAULT DETECTED - CHECK MOTOR")
        self._isFault = True
    
    def enable(self):
        '''
        @brief Enables the DRV8847 chip, allowing the motors to turn
        @details Note that this will enable all motors tied to the DRV8847 chip, 
        not just the 1 motor that is set up in the specific MotorDriver.MotorDriver object.
        Also, enable() must be called so that the motors can turn, otherwise they will not move.
        '''
        self._ext.disable()
        self.nSLEEP_pin.value(1)
        utime.sleep_us(50)
        self._ext.enable()
        
    def disable(self):
        '''
        @brief Disables the DRV8847 chip, stopping the motors from being able to be controlled.
        @details Note that this will disable all motors tied to the DRV8847 chip, 
        not just the 1 motor that is set up in the specific MotorDriver.MotorDriver object.
        '''
        
        self.nSLEEP_pin.low()
        
    def set_duty(self, duty):
        '''
        @brief Sets the duty cycle of the PWM signal
        @details This function takes a value from the inclusive interval -100 to 100.
        Positive values give forward motion, negative give reverse. To use the coast (fast decay)
        option, set the duty cycle to be 0. This will pulse one channel and leave the other at 0V. 
        
        @param duty The duty cycle of the PWM signal
        '''
        if not self.get_Fault():
            if duty > 100:
                duty = 100
                #print('Error: Duty Percentage out of range')
            elif duty < -100:
                duty = -100
            if duty >= 0:
                
                self.direction = 0
                
                self.motor_Channel_B.pulse_width_percent(0)
                
                self.motor_Channel_A.pulse_width_percent(duty)
                
            else:
                duty_corr = duty * -1
                
                self.direction = 1
                
                self.motor_Channel_A.pulse_width_percent(0)
                
                self.motor_Channel_B.pulse_width_percent(duty_corr)
                
        else:
            print('Motor in fault: Please clear fault and call reset_Fault()')
            
    def brake(self):
        '''
        @brief Brakes the motor according to the DRV8847 manual.
        @details This sets both channels to the high value. If the motor is spinning, 
        the first channel to be set high will be the channel that was previously at 0. 
        '''
        if not self.get_Fault():
            self._ext.disable()
            if self.direction:
                
                self.motor_Channel_A.pulse_width_percent(100)
                
                self.motor_Channel_B.pulse_width_percent(100)
                
            else:
                
                self.motor_Channel_B.pulse_width_percent(100)
                
                self.motor_Channel_A.pulse_width_percent(100)
            self._ext.enable()
        else:
            print('Motor in fault: Please clear fault and call reset_Fault()')   
    def get_Fault(self):
        '''
        @brief Returns the state of the motor driver
        @details This function returns true if the motor's fault condition has been triggered, false if it has not or if it has been
        reset
        
        @return The fault state as a boolean
        '''
        return self._isFault
    
    def reset_Fault(self):
        '''
        @brief Resets the fault state of the motor driver
        @details Call this function once the motor is no longer in a fault state. This will then reset system and reenable the motor.
        The function calls enable() internally.
        '''
        self._isFault = False
        self.enable()
## @cond        
if __name__ == "__main__":
    micropython.alloc_emergency_exception_buf(200)
    t = pyb.Timer(3, freq = 20000)
    a = MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2, pyb.Pin.cpu.B4, 1, pyb.Pin.cpu.B5, 2, t)
    
## @endcond