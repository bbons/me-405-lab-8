'''
@file Encoder.py

@brief Defines the driver for interfacing with a encoder on a pyboard
@details Full code for this file can be found at https://bitbucket.org/bbons/me-405-lab-8/src/master/Encoder.py .
@author Ben Bons and Ben Presley
@date Last Updated: March 10, 2021

'''
import pyb
import math

class Encoder:
    '''
    @brief Defines the encoder object that uses a timer channel to track a encoder position
    @details This class will interface with some pins on a pyboard and use that and a timer to track the position of an encoder. It also can be used to set the position of the encoder.
    
    '''
    
    def __init__(self, pin_ch1, pin_ch2, timer_int, prescaler, period):
        '''
        @brief Initializes the encoder object
        @details This function takes in the pins and timer number, and sets the prescaler and period of that timer. All this info is used to set up the timerChannel object
        to read the encoder. 
        
        @param pin_ch1 The pyb pin ID connected to channel one of the specific timer
        @param pin_ch2 The pyb pin ID connected to channel two of the specific timer
        @param timer_int The integer number of the timer being used, for which the two pin parameters are channel one and two
        @param prescaler The prescaler for the timer. It will take prescaler + 1 encoder ticks to trigger the timer
        @param period The period of the timer. The timer will rollover after period ticks.
        '''
        ## The timer object that is used for the 
        self.timer = pyb.Timer(timer_int, prescaler = prescaler, period = period)
        
        
        self.timer.channel(1, pin = pin_ch1, mode = pyb.Timer.ENC_AB)
        
        
        self.timer.channel(2, pin = pin_ch2, mode = pyb.Timer.ENC_AB)
        
        ## The position of the encoder
        self.position = 0
        
        ## The delta between two calls of update()
        self.delta = 0
        
        ## The period of the encoder
        self.period = period
        
    def update(self):
        '''
        @brief Updates the position of the encoder
        @details This method will update both the position and delta values of the encoder. It responds to overflow and underflow  of the timer. 
        '''
        
        x = self.timer.counter()
        #print(x)
        
        delta = x - self.position
        
        delta = self.fix_delta(delta, self.period)
        
        self.position += delta
        self.delta = delta
        
    def get_position(self):
        '''
        @brief Returns the current position of the encoder
        @return The current positon of the encoder
        '''
        
        return self.position
    
    def get_delta(self):
        '''
        @brief Returns the delta between the previous two update() commands
        @return The delta between two runs of update()
        '''
        return self.delta
        
    def set_position(self, new_pos):
        '''
        @brief Sets the position of the encoder and updates the timer to the correct value.
        @details This function will set the encoder position, and sets the timer to the value between 0 and the period that would correspond to that position. This essentially
        overflows or underflows the position.
        
        @param new_pos The new position of the encoder
        '''
        self.position = new_pos
        
        
        while new_pos<0:
            new_pos += (self.period+1)
        
        while (new_pos>self.period):
            new_pos -= (self.period+1)
        
        self.timer.counter(new_pos)
        
    def fix_delta(self, delta, period):
        '''
        @brief Fixes the delta between two update() commands
        @details This function corrects the delta due to underflow or overflow. This function compares the delta to half of the period, and if the delta is greater than that
        value, it is corrected. This is contingent on the position being updated regularly. 
        
        @param delta The delta to be corrected
        @param period The period to compare the delta to.
        
        @return The corrected delta
        '''
        while math.fabs(delta) > ((period + 1)/2):
            if delta > 0:
                delta = delta - period - 1
            else:
                delta =(period + 1 + delta)
        return delta