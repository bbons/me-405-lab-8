'''
@file balance_ball.py

@package term_prj

@brief Lab 0x09 - Compiling the Self-Balancing Ball 

@details This script runs the term project in its final form. The balancing \ref balance_ball.py file runs
the main script as well. The focus of the project is to implement strategies developed throughout
ME 305 and ME 405. Specifically, this lab includes a proportional control system -- which was developed in
this labratory to control the driver functions in the previous labs in \ref term_prj . The analytical solution ranged from kinematic relations
to 4th order state space equations of motion for our ball and platform interactions. 
With Ben Bons' knowledge of the place() function in MATLAB, 
and our collective knowledge control theory, we used MATLAB to solve for polar placement that would produce a critically damped 
step response. The place() function returned the proportional gains (the theoretical ones at least).
\n MATLAB code: https://bitbucket.org/bbons/me-405-lab-8/src/master/Project%20Files/polePlacement.mlx
\n \n As with all theoretical things, the real values within our control system had to be adjusted to
work with our examination of our system. The screen, IMU, and motors certainly had physical phenomena 
that simply could not be modeled. We then spent hours tuning our proportional gains to create the best possible results.
\n \n An video of the testing has been posted as a link below, which shows a testing run.
\n Test demonstration video: 
\n Source Code Link: https://bitbucket.org/bbons/me-405-lab-8/src/master/Project%20Files/balance_ball.py
\n bno055 documentation and code: https://github.com/micropython-IMU/micropython-bno055
    
\n This code was brought into existence in cooperation between Bens. \n

@author Ben Bons and Ben Presley

@date March 17th, 2021

'''

#------------------------------IMPORT LIBRARIES------------------------------#
import pyb
import micropython
import utime
from MotorDriver import MotorDriver #motor driver
from TouchScreen import TouchScreen #touchscreen driver
import machine #i2c machine module (for imu)
from bno055 import BNO055 #imu driver

#----------------------------CONTROL SYSTEM FSM------------------------------#
class BalanceControl:

    
    def __init__(self, motor1, motor2, screen, imu):
        '''
        @brief Initializes the controller object
        @details This is the initialization of the controller object class, which
        will hold responsibility for gathering data from the IMU, computing the corrections, 
        and executing the motor PWM ouputs. 
        \n The objects are passed into the controller to allow easier collaboration between
        my teammate and I; we were developing with slightly different motor drivers.
        @param motor1 The motor1 object
        @param motor2 The motor2 object
        @param screen The touchscreen object
        @param imu The IMU object
        '''
        #pass in the relevant objects
        ## callable motor1 object
        self.myMotor1 = motor1
        ## callable motor2 object
        self.myMotor2 = motor2
        ## callable screen object
        self.myScreen = screen
        ## callable imu object
        self.myIMU = imu
        
        #define gain controls without ball - originally analytically determined in MATLAB
        ## Proportional gain when ball is not touching for motor1
        self.gain_no_ball_1 = (0, -0.0088*5, 0, -0.1277*5)
        ## Proportional gain when ball is not touching for motor2
        self.gain_no_ball_2 = (0, -0.0088*4, 0, -0.1277*4)
        
        #define gain controls with ball - originally analytically  determined in MATLAB
        ## Proportional gain when ball is touching for motor1
        self.gain_ball_1 = (-1.8421*-0.3, -0.2119*0.1, -5.3842*-0.15, -3.6739*0.04)  #For short length control on touchscreen
        ## Proportional gain when ball is touching for motor2
        self.gain_ball_2 = (-1.8421*0.3, -0.2119*0.1, -5.3842*0.2, -3.6739*0.04)   #For long length control on touchscreen
        
        ## Torque to PWM conversion
        self.torque2pwm = 2.21*100/(0.0138*12) #converts a N-m to PWM duty
        
        #define the states of the FSM
        ## initialization state
        self.S0_init = 0
        ## control state
        self.S1_control = 1
        
        
        ## define state of finite state machine
        self.state = self.S0_init #start at FSM initialization state
        
        self.start_time = utime.ticks_us()
        
        ## data tuple from imu
        self.angular_pos = None
        
    def run(self):
        '''
        @brief The control system task
        @details This function will run within a while loop, it contains
        a finite state machine that is great for multitasking if there is a desire
        for front end communication relay. The function receives information from the imu and
        touchscreen, applies the controller gains, and then sets the PWM duty.
        '''
        #make sure SystemFault is false
        if not(self.myMotor1.get_Fault() or self.myMotor2.get_Fault()):
            
            if self.state == self.S0_init:
                self.myMotor1.enable()
                ## The time that the current execution of run() occured
                self.start_time = utime.ticks_us()
                self.state = self.S1_control
                self.angular_pos = self.myIMU.euler()
                self.pos = self.myScreen.get_All()
                
            elif self.state == self.S1_control:
                # Define time of execution
                delta = utime.ticks_diff(utime.ticks_us,self.start_time)
                self.start_time = utime.ticks_us()
                
                # Get Positions from IMU
                ## Store old angles
                _old_angle = self.angular_pos
                ## Data tuple from IMU driver
                self.angular_pos = self.myIMU.euler()
                
                #Find Angular speeds [deg/s]
                ## Angular speed of platform connected with motor1
                _angular_speed_1 = (self.angular_pos[1] - _old_angle[1])/delta * 1000000
                ## Angular speed of platform connected with motor2
                _angular_speed_2 = (self.angular_pos[2] - _old_angle[2])/delta * 1000000
                
                #Get Ball position (mm)
                _old_pos = self.pos
                ## data tuple from touchscreen
                self.pos = self.myScreen.get_All()
                
                # There is a weird error where the touchscreen thinks its being touched
                # at x = 97 or 98 mm. This code disregards those inputs, as it causes
                # many errors and is far from the center operating point
                if self.pos[0]>95:
                    self.pos = (0,0,0)
                
                #Get Ball speed (mm/s) -- X AND Y MAY BE SWITCHED DEPENDING ON YOUR TOUCHSCREEN OBJ.
                _y_speed = (self.pos[0] - _old_pos[0])/delta * 1000000
                _x_speed = (self.pos[1] - _old_pos[1])/delta * 1000000
                
                if self.pos[2]:
                     # Solve for the torque needed in N-m and PWM value for motor 1
                    _torque_1 = (self.gain_ball_1[1] * _angular_speed_1 + self.gain_ball_1[3] * self.angular_pos[1]) *3.14159/180 + (self.gain_ball_1[0] * _y_speed + self.gain_ball_1[2]*self.pos[0])/1000
                    _pwm_1 = _torque_1 * self.torque2pwm
                    
                    
                    # Solve for the torque needed in N-m and PWM value for motor 2
                    _torque_2 = (self.gain_ball_2[1] * _angular_speed_2 + self.gain_ball_2[3] * self.angular_pos[2]) *3.14159/180+ (self.gain_ball_2[0] * _x_speed + self.gain_ball_2[2]*self.pos[1])/1000
                    _pwm_2 = _torque_2 * self.torque2pwm
                    
                    #print('Y: Speed = {} mm/s, Angular Velocity = {} deg/s, Position = {} mm, Angle = {} deg, PWM = {}'.format(_y_speed, _angular_speed_1, self.pos[1], self.angular_pos[1], _pwm_1))
                    self.myMotor1.set_duty(_pwm_1)
                    
                    #print('X: Speed = {} mm/s, Angular Velocity = {} deg/s, Position = {} mm, Angle = {} deg, PWM = {}'.format(_x_speed, _angular_speed_2, self.pos[0], self.angular_pos[2], _pwm_2))
                    self.myMotor2.set_duty(_pwm_2)
                else:
                    # Solve for the torque needed in N-m and PWM value for motor 1
                    _torque_1 = (self.gain_no_ball_1[1] * _angular_speed_1 + self.gain_no_ball_1[3] * self.angular_pos[1]) *3.14159/180
                    _pwm_1 = _torque_1 * self.torque2pwm
                    
                    
                    # Solve for the torque needed in N-m and PWM value for motor 2
                    _torque_2 = (self.gain_no_ball_2[1] * _angular_speed_2 + self.gain_no_ball_2[3] * self.angular_pos[2]) *3.14159/180
                    _pwm_2 = _torque_2 * self.torque2pwm
                
                    #Control Motor 1
                    #print('Motor 1. Angle: {} deg, Angular Speed: {} deg/s, PWM: {}'.format(self.angular_pos[1], _angular_speed_1, _pwm_1))
                    self.myMotor1.set_duty(_pwm_1)
                    
                    #Control Motor 2
                    #print('Motor 2. Angle: {} deg, Angular Speed: {} deg/s, PWM: {}'.format(self.angular_pos[2], _angular_speed_2, _pwm_2))
                    self.myMotor2.set_duty(_pwm_2)
                
        else: #If SystemFault is true, then do not run!
            print('\nControl System Haulted due to SystemFault!\n')
            print('Angular Speed {}, Angle')
            pass
        
        
if __name__ == '__main__':
    ## timer for H-bridge and motors
    pwm_timer = pyb.Timer(3, freq = 20000)
    ## Allocate memory for fault detection
    micropython.alloc_emergency_exception_buf(200)
    ## global motor1 object to be assigned to controller object
    motor_1 = MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2, pyb.Pin.cpu.B4, 1, pyb.Pin.cpu.B5, 2, pwm_timer)
    ## global motor2 object to be assigned to controller object
    motor_2 = MotorDriver(pyb.Pin.cpu.A15, None, pyb.Pin.cpu.B0, 3, pyb.Pin.cpu.B1, 4, pwm_timer)
    ## global screen object to be assigned to controller object
    screen = TouchScreen(pyb.Pin.cpu.A6, pyb.Pin.cpu.A7, pyb.Pin.cpu.A1, pyb.Pin.cpu.A0, 2056, 3160, (2015,2045))
    ## i2c address if imu
    i2c = machine.I2C(1)
    ## global imu object to be assigned to controller object
    imu = BNO055(i2c)
    ## controller object task
    task = BalanceControl(motor_1, motor_2, screen, imu)
    while not motor_1.get_Fault():
        task.run()         