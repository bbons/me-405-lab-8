var classMotorDriver_1_1MotorDriver =
[
    [ "__init__", "classMotorDriver_1_1MotorDriver.html#a0d2c72774716522cf1aab02cb0f53c1b", null ],
    [ "brake", "classMotorDriver_1_1MotorDriver.html#af6121d651be76d76793aab42b607a3ba", null ],
    [ "disable", "classMotorDriver_1_1MotorDriver.html#abb9a67928c8ed29dd06b04727813411a", null ],
    [ "enable", "classMotorDriver_1_1MotorDriver.html#a296e591519f90c295ca618e961baa1a7", null ],
    [ "get_Fault", "classMotorDriver_1_1MotorDriver.html#ae19cf6e6047b4a9cc898d8c0e716bf1b", null ],
    [ "reset_Fault", "classMotorDriver_1_1MotorDriver.html#a85423dc358d82530310bc2ce909d694b", null ],
    [ "set_duty", "classMotorDriver_1_1MotorDriver.html#a4bb86eafa05d8e874896aef624ad14cd", null ],
    [ "direction", "classMotorDriver_1_1MotorDriver.html#a7acb0da3eb2e7782409f2c9820ed058f", null ],
    [ "motor_Channel_A", "classMotorDriver_1_1MotorDriver.html#a351d65c77e107910f4be70542a0b2365", null ],
    [ "motor_Channel_B", "classMotorDriver_1_1MotorDriver.html#a754394336b9a0e9fd8a44cf5ab3ee2dc", null ],
    [ "nFAULT_pin", "classMotorDriver_1_1MotorDriver.html#aa4668bba19a96effa0495bf17a6f4d47", null ],
    [ "nSLEEP_pin", "classMotorDriver_1_1MotorDriver.html#a23e6a5c19063b2d8ab0d5aa205f7ee94", null ],
    [ "timer", "classMotorDriver_1_1MotorDriver.html#ab01a28fc3b6e0720c1d9922ac16a4010", null ]
];